from cms.plugin_base import CMSPluginBase
from django.contrib import admin
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _
from .models import (
    Header, SlideModel, SliderModel, BlockTitleModel,
    StoryBoxContainerModel, StoryBoxModel, CountDownModel,
    PartyBlockModel, SectionModel, TitledParagraphModel,
    ParticipantModel
)


class HeaderPlugin(CMSPluginBase):
    model = Header
    name = _('Page Header')
    render_template = 'plugins/headers/page-header.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(HeaderPlugin, self).render(context, instance, placeholder)
        return context
plugin_pool.register_plugin(HeaderPlugin)

class TitledParagraphPlugin(CMSPluginBase):
    model = TitledParagraphModel
    name = _('Titled Paragraph')
    render_template = 'plugins/paragraph/titled-paragraph.html'
    cache = False
    allow_children = True

    def render(self, context, instance, placeholder):
        context = super(TitledParagraphPlugin, self).render(context, instance, placeholder)
        return context
plugin_pool.register_plugin(TitledParagraphPlugin)

class SliderPlugin(CMSPluginBase):
    model = SliderModel
    name = _('Slider')
    render_template = 'plugins/slider/slider.html'
    cache = True
    allow_children = True
    child_classes = ['SlidePlugin']

    def render(self, context, instance, placeholder):
        context = super(SliderPlugin, self).render(context, instance, placeholder)
        return context
plugin_pool.register_plugin(SliderPlugin)

class SlidePlugin(CMSPluginBase):
    model = SlideModel
    name = _('Slide')
    render_template = 'plugins/slider/slide.html'
    cache = True
    require_parent = True
    parent_classes = ['SliderPlugin']

    def render(self, context, instance, placeholder):
        context = super(SlidePlugin, self).render(context, instance, placeholder)
        return context
plugin_pool.register_plugin(SlidePlugin)

class BlockTitlePlugin(CMSPluginBase):
    model = BlockTitleModel
    name = _('Block Title')
    render_template = 'plugins/blocks/block-title.html'
    cache = False

    def render(self, context, instance, placeholder):
        context = super(BlockTitlePlugin, self).render(context, instance, placeholder)
        return context
plugin_pool.register_plugin(BlockTitlePlugin)

class StoryBoxContainerPlugin(CMSPluginBase):
    model = StoryBoxContainerModel
    name = _('Story Box Container')
    render_template = 'plugins/story/story-container.html'
    cache = False
    allow_children = True
    child_classes = ['StoryBoxPlugin']

    def render(self, context, instance, placeholder):
        context = super(StoryBoxContainerPlugin, self).render(context, instance, placeholder)
        return context
plugin_pool.register_plugin(StoryBoxContainerPlugin)

class StoryBoxPlugin(CMSPluginBase):
    model = StoryBoxModel
    name = _('Story Box')
    render_template = 'plugins/story/story-box.html'
    cache = False
    require_parent = True
    parent_classes = ['StoryBoxContainerPlugin']

    def render(self, context, instance, placeholder):
        context = super(StoryBoxPlugin, self).render(context, instance, placeholder)
        return context
plugin_pool.register_plugin(StoryBoxPlugin)

class CountDownPlugin(CMSPluginBase):
    model = CountDownModel
    name = _('Count Down')
    render_template = 'plugins/countdown/count-down.html'
    cache = False
    allow_children = True

    def render(self, context, instance, placeholder):
        context = super(CountDownPlugin, self).render(context, instance, placeholder)
        return context
plugin_pool.register_plugin(CountDownPlugin)

class SectionPlugin(CMSPluginBase):
    model = SectionModel
    name = _('Section')
    render_template = 'plugins/section/section.html'
    cache = False
    allow_children = True

    def render(self, context, instance, placeholder):
        context = super(SectionPlugin, self).render(context, instance, placeholder)
        return context
plugin_pool.register_plugin(SectionPlugin)

class PartyBlockModelPlugin(CMSPluginBase):
    model = PartyBlockModel
    name = _('Party Block')
    render_template = 'plugins/partyblocks/party-block.html'
    cache = False
    allow_children = True

    def render(self, context, instance, placeholder):
        context = super(PartyBlockModelPlugin, self).render(context, instance, placeholder)
        return context
plugin_pool.register_plugin(PartyBlockModelPlugin)

class ParticipantPlugin(CMSPluginBase):
    model = ParticipantModel
    name = _('Participant')
    render_template = 'plugins/participant/participant.html'
    cache = False
    require_parent = True
    parent_classes = ['PartyBlockModelPlugin']

    def render(self, context, instance, placeholder):
        context = super(ParticipantPlugin, self).render(context, instance, placeholder)
        return context
plugin_pool.register_plugin(ParticipantPlugin)