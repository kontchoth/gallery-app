from django.template.loader import render_to_string
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _
from djangocms_text_ckeditor.fields import HTMLField
from colorfield.fields import ColorField

from django.db import models

SHORT_MESSAGE = 300

class Header(CMSPlugin):
    husband = models.CharField(max_length=100, blank=False, null=False)
    husband_name_color = ColorField(default='#000');
    wife = models.CharField(max_length=100, blank=False, null=False)
    husband_name_color = ColorField(default='#000');
    celebration_data = models.DateTimeField(blank=False, null=False)

    def __str__(self):
        return 'Page Header'


# Slider Model
class SlideModel(CMSPlugin):
    name = models.CharField(max_length=255, blank=True, null=True)
    image = models.ImageField(upload_to='images/slider')
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name if self.name else 'Slide'

class SliderModel(CMSPlugin):
    height = models.IntegerField(default=100)
    story = models.TextField(blank=True, null=True)
    slide_duration = models.IntegerField(blank=False, null=False, default=3000)

    def __str__(self):
        return 'Slider'

# Block Title Model
class BlockTitleModel(CMSPlugin):
    title = models.CharField(max_length=255, blank=False, null=False)
    line_color = ColorField(default='white')
    def __str__(self):
        return self.title


# story box
class StoryBoxModel(CMSPlugin):
    author = models.CharField(max_length=255, blank=False, null=False)
    message = HTMLField(blank=False, null=False)

    def __str__(self):
        return 'Story from {}'.format(self.author)

    def get_short_message(self):
        return self.message[:SHORT_MESSAGE] + '...'

    def get_full_message(self):
        print(dir(self.message))
        return str(self.message)

class StoryBoxContainerModel(CMSPlugin):
    
    def __str__(self):
        return 'Story Box Container'

# count down
class CountDownModel(CMSPlugin):
    title = models.CharField(max_length=255, blank=False, null=False, default='Count Down')
    background_color = ColorField(default='#8a8e90')
    date = models.DateTimeField(blank=False, null=False)

    def __str__(self):
        return 'Count Down'

# Participant
class SectionModel(CMSPlugin):
    background_color = ColorField(default='#fff')

    def __str__(self):
        return 'Section Block'

# Participant
class PartyBlockModel(CMSPlugin):
    def __str__(self):
        return 'Party Block'

# Titled Paragraph
class TitledParagraphModel(CMSPlugin):
    title = models.CharField(max_length=255, null=False, blank=False)
    content = models.TextField(blank=False, null=False, default='')

    def __str__(self):
        return 'Titled Paragraph'

# participant model
class ParticipantModel(CMSPlugin):
    name = models.CharField(max_length=255, null=False, blank=False)
    role = models.CharField(max_length=255, null=False, blank=False)
    image = models.ImageField(upload_to='images/participants')
    bio = models.TextField(default='')

    def __str__(self):
        return 'Participant {}'.format(self.name)

